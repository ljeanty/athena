#include "TRT_TrackSegmentsTool_xk/TRT_TrackSegmentsMaker_ATLxk.h"
#include "TRT_TrackSegmentsTool_xk/TRT_TrackSegmentsMaker_ECcosmics.h"
#include "TRT_TrackSegmentsTool_xk/TRT_TrackSegmentsMaker_BarrelCosmics.h"

using namespace InDet;

DECLARE_COMPONENT( TRT_TrackSegmentsMaker_ATLxk )
DECLARE_COMPONENT( TRT_TrackSegmentsMaker_ECcosmics )
DECLARE_COMPONENT( TRT_TrackSegmentsMaker_BarrelCosmics )

