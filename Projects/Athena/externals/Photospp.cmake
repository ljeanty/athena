#
# File specifying the location of Photos++ to use.
#

set( PHOTOSPP_LCGVERSION 3.61 )
set( PHOTOSPP_LCGROOT
   ${LCG_RELEASE_DIR}/MCGenerators/photos++/${PHOTOSPP_LCGVERSION}/${LCG_PLATFORM} )
